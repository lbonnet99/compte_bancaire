package compte_bancaire;


public class compte {
    private int solde;

    public compte(int solde) throws soldenegException{
        if(solde<0)
            throw new soldenegException();
        else
            this.solde = solde;
    }

    public int getSolde() {
        return solde;
    }

    public void crediter(int somme) throws sommenegException{
        if(somme<0)
            throw new sommenegException();
        else
            solde = solde + somme;
    }

    public void debiter(int somme) throws sommenegException{
        if(somme<0)
            throw new sommenegException();
        else
            solde = solde -somme;
    }

    public void virement(compte c1, int somme) throws sommenegException{
        this.debiter(somme);
        c1.crediter(somme);
    }

}

